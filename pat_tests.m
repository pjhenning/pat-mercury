:- module pat_tests.

:- interface.

:- import_module io.

:- pred main(io::di, io::uo) is det.

:- implementation.

:- use_module policy, string.
:- import_module list.

main(!IO) :-
	io.print_line("Testing...", !IO),
	test_rule_parsing(!IO).

test_rule_parsing(!IO) :-
	list.map_foldl(
		pred({IV, TV}::in, {OV, TV}::out, IOI::di, IOF::uo) is det :- (
			R = policy.parse(IV, IOI, IOF),
			OV = string.string(R)
		),
		Parse_tests,
		Parse_results,
		!IO
	),
	list.filter(pred({A, B}::in) is semidet :- A \= B, Parse_results, Parse_failures),
	( 
		if list.is_not_empty(Parse_failures) then (
			print_line("The following parse tests failed:", !IO),
			print_line(Parse_failures, !IO)
		)
		else print_line("Parse tests succeeded :)", !IO)
	),
	Parse_tests = [
		{
			"can_revoke(false, President)",
			"[cr(bool(f), ""President"")]"
		},
		{
			"can_assign(President, Staff, President)",
			"[ca(""President"", preconds(sol([pos(""Staff"")])), ""President"")]"
		},
		{
			"RH(President, VicePresident)",
			"[rh(""President"", ""VicePresident"")]"
		},
		{
			"UA(u1, President)",
			"[ua(""u1"", ""President"")]"
		},
		{
			"SMER(A, B)",
			"[smer(""A"", ""B"")]"
		},
		{
			"PA(President, eat, Hamberders)",
			"[pa(""President"", ""eat"", ""Hamberders"")]"
		}
	].
