:- module policy.hierarchy.

:- interface.

:- pred senior(role_dynamic, role_dynamic, list(rh)).
:- mode senior(in,           out,          in) is nondet.
:- mode senior(out,          in,           in) is nondet.
:- mode senior(out,          out,          in) is nondet.

:- pred senior_in_common(
	role_dynamic::in,
	role_dynamic::in,
	list(rh)::in
) is semidet.

:- func seniors(role_dynamic, list(rh)) = list(role_dynamic).

:- func juniors(role_dynamic, list(rh)) = list(role_dynamic).

:- pred senior_smer(
	smer::in,
	list(rh)::in,
	smer::out
) is nondet.

:- implementation.

senior(Senior, Junior, Hierarchy) :-
	member(rh(Senior, Junior), Hierarchy).

senior_in_common(R1, R2, Hierarchy) :-
	senior(S, R1, Hierarchy),
	senior(S, R2, Hierarchy).

seniors(Role, Hierarchy) = Seniors :-
	solutions(
		pred(S::out) is nondet :- senior(S, Role, Hierarchy),
		Seniors
	).

juniors(Role, Hierarchy) = Juniors :-
	solutions(
		pred(J::out) is nondet :- senior(Role, J, Hierarchy),
		Juniors
	).

% TODO?: Make this more declarative?
senior_smer(smer(A, B), Hierarchy, smer(A_Or_Senior, B_Or_Senior)) :-
	AS = [A | seniors(A, Hierarchy)],
	BS = [B | seniors(B, Hierarchy)],
	member(A_Or_Senior, AS),
	member(B_Or_Senior, BS),
	((A_Or_Senior = A) => (B_Or_Senior \= B)),
	((B_Or_Senior = B) => (A_Or_Senior \= A)).
