:- module policy.utils.

:- interface.

:- pred disjoint(set(T)::in, set(T)::in) is semidet.

:- implementation.

disjoint(A, B) :-
	Common = intersect(A, B),
	is_empty(Common).
