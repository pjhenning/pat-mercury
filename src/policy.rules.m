:- module policy.rules.

:- interface.

:- pred collect_rule_data(
	rule_static::in, 
	policy_dynamic::in, 
	policy_dynamic::out,
	io::di,
	io::uo
) is det.

:- func get_all(rules_dynamic::in, io::di, io::uo) = (list(rule_dynamic)::out) is det.

:- func get_all_static(rules_dynamic::in, io::di, io::uo) = (list(rule_static)::out) is det.

:- func get_cas(rules_dynamic::in, io::di, io::uo) = (list(ca)::out) is det.

:- pred filter_cas(
	pred(ca)::in(pred(in) is semidet),
	list(ca_ref)::in,
	list(ca_ref)::out,
	io::di,
	io::uo
) is det.

:- pred without_ca_rules(
	list(ca_ref)::in,
	policy_dynamic::in,
	policy_dynamic::out,
	io::di,
	io::uo
) is det.

:- pred without_cas_matching(
	pred(ca)::in(pred(in) is semidet),
	policy_dynamic::in,
	policy_dynamic::out,
	io::di,
	io::uo
) is det.

:- func get_crs(rules_dynamic::in, io::di, io::uo) = (list(cr)::out) is det.

:- pred without_cr_rules(
	list(cr_ref)::in,
	policy_dynamic::in,
	policy_dynamic::out,
	io::di,
	io::uo
) is det.

:- pred without_crs_matching(
	pred(cr)::in(pred(in) is semidet),
	policy_dynamic::in,
	policy_dynamic::out,
	io::di,
	io::uo
) is det.

:- pred filter_crs(
	pred(cr)::in(pred(in) is semidet),
	list(cr_ref)::in,
	list(cr_ref)::out,
	io::di,
	io::uo
) is det.

:- func get_rhs(rules_dynamic::in, io::di, io::uo) = (list(rh)::out) is det.

:- func get_smers(rules_dynamic::in, io::di, io::uo) = (list(smer)::out) is det.

:- func get_uas(rules_dynamic::in, io::di, io::uo) = (list(ua)::out) is det.

:- func get_pas(rules_dynamic::in, io::di, io::uo) = (list(pa)::out) is det.

:- pred without_rule(rule_ref::in, policy_dynamic::in, policy_dynamic::out) is det.

:- func get_ca(ca_ref::in, io::di, io::uo) = (ca::out) is det.

:- func get_rule_ca(ca_ref::in, io::di, io::uo) = (rule_dynamic::out) is det.

:- pred with_ca(
	ca_static::in,
	policy_dynamic::in,
	policy_dynamic::out,
	io::di,
	io::uo
) is det.

:- pred set_ca(ca_ref::in, ca::in, io::di, io::uo) is det.

:- pred try_match_ca(
	pred(role_occurrence, role_dynamic, ca, T)::in(pred(in, in, in, out) is semidet),
	role_occurrence::in,
	role_dynamic::in,
	ca::in,
	list(T)::in,
	list(T)::out
) is det.

:- func get_cr(cr_ref::in, io::di, io::uo) = (cr::out) is det.

:- func get_rule_cr(cr_ref::in, io::di, io::uo) = (rule_dynamic::out) is det.

:- pred with_cr(
	cr_static::in,
	policy_dynamic::in,
	policy_dynamic::out,
	io::di,
	io::uo
) is det.

:- func get_rh(rh_ref::in, io::di, io::uo) = (rh::out) is det.

:- func get_rule_rh(rh_ref::in, io::di, io::uo) = (rule_dynamic::out) is det.

:- pred with_rh(
	rh_static::in,
	policy_dynamic::in,
	policy_dynamic::out,
	io::di,
	io::uo
) is det.

:- pred with_smer(
	smer_static::in,
	policy_dynamic::in,
	policy_dynamic::out,
	io::di,
	io::uo
) is det.

:- pred with_ua(
	ua_static::in,
	policy_dynamic::in,
	policy_dynamic::out,
	io::di,
	io::uo
) is det.

:- pred filter_map_uas_set(
	pred(ua, T)::in(pred(in, out) is semidet),
	policy_dynamic::in,
	set(T)::out,
	io::di,
	io::uo
) is det.

:- pred with_pa(
	pa_static::in,
	policy_dynamic::in,
	policy_dynamic::out,
	io::di,
	io::uo
) is det.


:- implementation.

collect_rule_data(Rule, !Policy, !IO) :-
	(
		Rule = ca(A, P, T),
		with_ca(ca(A, P, T), !Policy, !IO)
	;
		Rule = cr(A, B),
		with_cr(cr(A, B), !Policy, !IO)
	;
		Rule = rh(S, J),
		with_rh(rh(S, J), !Policy, !IO)
	;
		Rule = smer(A, B),
		with_smer(smer(A, B), !Policy, !IO)
	;
		Rule = ua(U, R),
		with_ua(ua(U, R), !Policy, !IO)
	;
		Rule = pa(R, P, O),
		with_pa(pa(R, P, O), !Policy, !IO)
	).

get_all(RuleRefs, !IO) = All :-
	list.map_foldl(
		pred(RuleRef::in, RuleOut::out, !.IO2::di, !:IO2::uo) is det :-
		( 
			rule_ref(Ref) = RuleRef,
			get_mutvar(Ref, RuleVal, !IO2),
			RuleOut = as_rule(RuleVal)
		),
		RuleRefs ^ all_rules,
		All,
		!.IO,
		!:IO
	).

get_all_static(RuleRefs, !IO) = AllStatic :-
	list.map_foldl(
		pred(RuleRef::in, RuleStatic::out, !.IO2::di, !:IO2::uo) is det :-
		(
			rule_ref(Ref) = RuleRef,
			get_mutvar(Ref, RuleVal, !IO2),
			RuleDyn = as_rule(RuleVal),
			RuleStatic = to_static(RuleDyn)
		),
		RuleRefs ^ all_rules,
		AllStatic,
		!.IO,
		!:IO
	).

:- func to_static(rule_dynamic::in) = (rule_static::out) is det.
to_static(RuleDyn) = RuleStatic :-
	(
		RuleDyn = ca(Admin, PC, Target),
		(
			PC = bool(B),
			PC_Names = bool(B)
		;
			PC = preconds(PC_Roles),
			roles.preconds_dyn_to_static(PC_Roles, PC_RolesStatic),
			PC_Names = preconds(PC_RolesStatic)
		),
		RuleStatic = ca(Admin ^ name, PC_Names, Target ^ name)
	;
		RuleDyn = cr(BoolOrAdmin, Target),
		(
			BoolOrAdmin = bool(B),
			BoA = bool(B)
		;
			BoolOrAdmin = role(R),
			BoA = role(R ^ name)
		),
		RuleStatic = cr(BoA, Target ^ name)
	;
		RuleDyn = rh(S, J),
		RuleStatic = rh(S ^ name, J ^ name)
	;
		RuleDyn = smer(A, B),
		RuleStatic = smer(A ^ name, B ^ name)
	;
		RuleDyn = ua(U, R),
		RuleStatic = ua(U, R ^ name)
	;
		RuleDyn = pa(R, P, O),
		RuleStatic = pa(R ^ name, P, O)
	).

get_cas(Rules, !IO) = CAs :-
	get_cas(Rules ^ ca_rules, CAs, !IO).

:- pred get_cas(list(ca_ref)::in, list(ca)::out, io::di, io::uo) is det.
get_cas(CA_Refs, CAs, !IO) :-
	list.map_foldl(
		pred(RuleRef::in, RuleOut::out, I::di, O::uo) is det :-
			RuleOut = get_ca(RuleRef, I, O),
		CA_Refs,
		CAs,
		!IO
	).

:- func get_ca_refs_rules(list(ca_ref)::in, io::di, io::uo) =
	(list(rule_dynamic)::out) is det.
get_ca_refs_rules(CA_Refs, !IO) = Rules :-
	list.map_foldl(
		pred(CA_Ref::in, RuleOut::out, I::di, O::uo) is det :-
			RuleOut = as_rule(get_ca(CA_Ref, I, O)),
		CA_Refs,
		Rules,
		!IO
	).

without_ca_rules(CA_Refs, !Policy, !IO) :-
	% Remove rule references from 'all_rules' list
	write_string("Removing refs: ", !IO),
	write_doc(format(CA_Refs), !IO),
	write_string("\nWith vals: ", !IO),
	get_cas(CA_Refs, Withouts, !IO),
	write_doc(format(Withouts), !IO),
	list.foldl2(
		pred(
			RuleRef::in,
			!.Rules::in,
			!:Rules::out,
			!.IO2::di,
			!:IO2::uo
		) is det :-
		(
			rule_ref(R) = RuleRef,
			get_mutvar(R, PreVal, !IO2),
			Val = as_rule(PreVal),
			CAs = get_ca_refs_rules(CA_Refs, !.IO2, !:IO2),
			(
				if (not list.member(Val, CAs)) then
					!:Rules = [RuleRef | !.Rules]
				else true
			)
		),
		!.Policy ^ rules ^ all_rules,
		[],
		RulesWithout,
		!IO
	),
	!Policy ^ rules ^ all_rules := RulesWithout,
	% Remove rule references from 'ca_rules' list
	list.foldl2(
		pred(CA_Ref::in, !.Rem::in, !:Rem::out, !.IO2::di, !:IO2::uo) is det :-
		(
			CurrentCA = get_ca(CA_Ref, !.IO2, !:IO2),
			(
				if not list.member(CurrentCA, Withouts) then
					!:Rem = [CA_Ref | !.Rem]
				else true
			)
		),
		!.Policy ^ rules ^ ca_rules,
		[],
		RemRules,
		!IO
	),
	!Policy ^ rules ^ ca_rules := RemRules,
	write_string("\nRules left: ", !IO),
	write_doc(format(RemRules), !IO),
	write_string("\n", !IO),
	% Remove occurrence references from role objects
	list.foldl(
		pred(CA_Ref::in, !.IO2::di, !:IO2::uo) is det :-
		(
			ca(Admin, BoolOrPreconds, Target) = get_ca(CA_Ref, !.IO2, !:IO2),
			without_occurrence(Admin, ca_admin(CA_Ref), !IO2),
			without_occurrence(Target, ca_target(CA_Ref), !IO2),
			(
				if BoolOrPreconds = preconds(PC) then
					without_precond_occurrences(PC, CA_Ref, !IO2)
				else true
			)
		),
		CA_Refs,
		!IO
	).

% TODO: Refactor this to be more efficient
without_cas_matching(Test, !Policy, !IO) :-
	filter_cas(Test, !.Policy ^ rules ^ ca_rules, Matches, !IO),
	without_ca_rules(Matches, !Policy, !IO).

filter_cas(Test, InRefs, OutRefs, !IO) :-
	list.foldl2(
		pred(CA_Ref::in, !.Outs::in, !:Outs::out, I::di, O::uo) is det :-
		(
			CA_Val = get_ca(CA_Ref, I, O),
			(
				if Test(CA_Val) then
					!:Outs = [CA_Ref | !.Outs]
				else true
			)
		),
		InRefs,
		[],
		OutRefs,
		!IO
	).

get_crs(Rules, !IO) = CRs :-
	list.map_foldl(
		pred(RuleRef::in, RuleOut::out, I::di, O::uo) is det :-
			RuleOut = get_cr(RuleRef, I, O),
		Rules ^ cr_rules,
		CRs,
		!.IO,
		!:IO
	).

:- func get_cr_refs_rules(list(cr_ref)::in, io::di, io::uo) =
	(list(rule_dynamic)::out) is det.
get_cr_refs_rules(CR_Refs, !IO) = Rules :-
	list.map_foldl(
		pred(CR_Ref::in, RuleOut::out, I::di, O::uo) is det :-
			RuleOut = as_rule(get_cr(CR_Ref, I, O)),
		CR_Refs,
		Rules,
		!.IO,
		!:IO
	).

without_cr_rules(CR_Refs, !Policy, !IO) :-
	list.foldl2(
		pred(
			RuleRef::in,
			!.Rules::in,
			!:Rules::out,
			!.IO2::di,
			!:IO2::uo
		) is det :-
		(
			rule_ref(R) = RuleRef,
			get_mutvar(R, PreVal, !IO2),
			Val = as_rule(PreVal),
			CRs = get_cr_refs_rules(CR_Refs, !.IO2, !:IO2),
			(
				if (not list.member(Val, CRs)) then
					!:Rules = [RuleRef | !.Rules]
				else true
			)
		),
		!.Policy ^ rules ^ all_rules,
		[],
		RulesWithout,
		!IO
	),
	!Policy ^ rules ^ all_rules := RulesWithout,
	!Policy ^ rules ^ cr_rules :=
		list.delete_elems(!.Policy ^ rules ^ cr_rules, CR_Refs),
	list.foldl(
		pred(CR_Ref::in, !.IO2::di, !:IO2::uo) is det :-
		(
			cr(BoolOrRole, Target) = get_cr(CR_Ref, !.IO2, !:IO2),
			without_occurrence(Target, cr_target(CR_Ref), !IO2),
			(
				if BoolOrRole = role(R) then
					without_occurrence(R, cr_admin(CR_Ref), !IO2)
				else true
			)
		),
		CR_Refs,
		!IO
	).

% TODO: Refactor this to be more efficient
without_crs_matching(Test, !Policy, !IO) :-
	filter_crs(Test, !.Policy ^ rules ^ cr_rules, Matches, !IO),
	without_cr_rules(Matches, !Policy, !IO).

filter_crs(Test, InRefs, OutRefs, !IO) :-
	list.foldl2(
		pred(CR_Ref::in, !.Outs::in, !:Outs::out, I::di, O::uo) is det :-
		(
			CR_Val = get_cr(CR_Ref, I, O),
			(
				if Test(CR_Val) then
					!:Outs = [CR_Ref | !.Outs]
				else true
			)
		),
		InRefs,
		[],
		OutRefs,
		!IO
	).

get_rhs(Rules, !IO) = RHs :-
	list.map_foldl(
		pred(RuleRef::in, RuleOut::out, I::di, O::uo) is det :-
		(
			rh_ref(Ref) = RuleRef,
			get_mutvar(Ref, RuleVal, I, O),
			RuleOut = as_rh(RuleVal)
		),
		Rules ^ rh_rules,
		RHs,
		!.IO,
		!:IO
	).

get_smers(Rules, !IO) = SMERs :-
	list.map_foldl(
		pred(RuleRef::in, RuleOut::out, I::di, O::uo) is det :-
		(
			smer_ref(Ref) = RuleRef,
			get_mutvar(Ref, RuleVal, I, O),
			RuleOut = as_smer(RuleVal)
		),
		Rules ^ smer_rules,
		SMERs,
		!.IO,
		!:IO
	).

get_uas(Rules, !IO) = UAs :-
	list.map_foldl(
		pred(RuleRef::in, RuleOut::out, I::di, O::uo) is det :-
		(
			ua_ref(Ref) = RuleRef,
			get_mutvar(Ref, RuleVal, I, O),
			RuleOut = as_ua(RuleVal)
		),
		Rules ^ ua_rules,
		UAs,
		!.IO,
		!:IO
	).

get_pas(Rules, !IO) = PAs :-
	list.map_foldl(
		pred(RuleRef::in, RuleOut::out, I::di, O::uo) is det :-
		(
			pa_ref(Ref) = RuleRef,
			get_mutvar(Ref, RuleVal, I, O),
			RuleOut = as_pa(RuleVal)
		),
		Rules ^ pa_rules,
		PAs,
		!.IO,
		!:IO
	).

without_rule(Rule, !Policy) :-
	list.delete_all(!.Policy ^ rules ^ all_rules, Rule, RulesNext),
	!Policy ^ rules ^ all_rules := RulesNext.

get_ca(ca_ref(Ref), !IO) = as_ca(CA) :-
	get_mutvar(Ref, CA, !IO).

get_rule_ca(ca_ref(Ref), !IO) = as_rule(CA) :-
	get_mutvar(Ref, CA, !IO).

with_ca(ca(AdminName, StaticBoolOrPreconds, TargetName), !Policy, !IO) :-
	with_role(AdminName, Admin, !Policy, !IO),
	with_role(TargetName, Target, !Policy, !IO),
	(
		StaticBoolOrPreconds = bool(_),
		StaticPreconds = set.init
	;
		StaticBoolOrPreconds = preconds(P),
		StaticPreconds = P
	),
	set.foldl3(with_pcrole, StaticPreconds, set.init, Preconds, !Policy, !IO),
	(
		StaticBoolOrPreconds = bool(B),
		CA = ca(Admin, bool(B), Target)
	;
		StaticBoolOrPreconds = preconds(_),
		CA = ca(Admin, preconds(Preconds), Target)
	),
	new_mutvar(CA, Ref, !IO),
	CA_Ref = 'new ca_ref'(Ref),
	R_Ref = 'new rule_ref'(Ref),
	!Policy ^ rules ^ ca_rules := [CA_Ref | !.Policy ^ rules ^ ca_rules],
	!Policy ^ rules ^ all_rules := [R_Ref | !.Policy ^ rules ^ all_rules],
	with_occurrence(Admin, ca_admin(CA_Ref), !IO),
	set.foldl(
		pred(Precond::in, I::di, O::uo) is det :-
		(
			Precond = pos(R),
			with_occurrence(R, pos_precond(CA_Ref), I, O)
		;
			Precond = neg(R),
			with_occurrence(R, neg_precond(CA_Ref), I, O)
		),
		Preconds,
		!IO
	),
	with_occurrence(Target, ca_target(CA_Ref), !IO).

/* TODO?: come up with a less fragile approach to this */
% TODO: Write some code to test that this works in .net environment
set_ca(CA_Ref, NewVal, !IO) :-
	ca_ref(Candidate) = CA_Ref,
	(
		if dynamic_cast(Candidate, Ref) then
			set_mutvar(Ref, NewVal, !IO)
		else
			throw(software_error("You should never see this message"))
	).

try_match_ca(Test, Occurrence, Role, CA_Val, !OutVals) :-
	if Test(Occurrence, Role, CA_Val, OutVal) then
		!:OutVals = [OutVal | !.OutVals]
	else true.

get_cr(CR_Ref, !IO) = as_cr(CR) :-
	cr_ref(Ref) = CR_Ref,
	get_mutvar(Ref, CR, !IO).

get_rule_cr(cr_ref(Ref), !IO) = as_rule(CR) :-
	get_mutvar(Ref, CR, !IO).

with_cr(cr(BoolOrRoleStatic, TargetName), !Policy, !IO) :-
		with_role(TargetName, Target, !Policy, !IO),
		(
			BoolOrRoleStatic = bool(B),
			BoR = bool(B)
		;
			BoolOrRoleStatic = role(RoleName),
			with_role(RoleName, Role, !Policy, !IO),
			BoR = role(Role)
		),
		new_mutvar(cr(BoR, Target), Ref, !IO), 
		CR = 'new cr_ref'(Ref),
		R = 'new rule_ref'(Ref),
		!Policy ^ rules ^ cr_rules := [CR | !.Policy ^ rules ^ cr_rules],
		!Policy ^ rules ^ all_rules := [R | !.Policy ^ rules ^ all_rules],
		with_occurrence(Target, cr_target(CR), !IO),
		(
			if BoR = role(DynRole) then
				with_occurrence(DynRole, cr_admin(CR), !IO)
			else true
		).

get_rh(rh_ref(Ref), !IO) = as_rh(RH) :-
	get_mutvar(Ref, RH, !IO).

get_rule_rh(rh_ref(Ref), !IO) = as_rule(RH) :-
	get_mutvar(Ref, RH, !IO).

with_rh(rh(SeniorName, JuniorName), !Policy, !IO) :-
	with_role(SeniorName, Senior, !Policy, !IO),
	with_role(JuniorName, Junior, !Policy, !IO),
	new_mutvar(rh(Senior, Junior), Ref, !IO),
	RH = 'new rh_ref'(Ref),
	R = 'new rule_ref'(Ref),
	!Policy ^ rules ^ rh_rules := [RH | !.Policy ^ rules ^ rh_rules],
	!Policy ^ rules ^ all_rules := [R | !.Policy ^ rules ^ all_rules],
	with_occurrence(Senior, rh_senior(RH), !IO),
	with_occurrence(Junior, rh_junior(RH), !IO).

with_smer(smer(A_Name, B_Name), !Policy, !IO) :-
	with_role(A_Name, A, !Policy, !IO),
	with_role(B_Name, B, !Policy, !IO),
	new_mutvar(smer(A, B), Ref, !IO),
	SMER = 'new smer_ref'(Ref),
	R = 'new rule_ref'(Ref),
	!Policy ^ rules ^ smer_rules := [SMER | !.Policy ^ rules ^ smer_rules],
	!Policy ^ rules ^ all_rules := [R | !.Policy ^ rules ^ all_rules].

with_ua(ua(User, RoleName), !Policy, !IO) :-
	with_user(User, !Policy),
	with_role(RoleName, Role, !Policy, !IO),
	new_mutvar(ua(User, Role), Ref, !IO),
	UA = 'new ua_ref'(Ref),
	R = 'new rule_ref'(Ref),
	!Policy ^ rules ^ ua_rules := [UA | !.Policy ^ rules ^ ua_rules],
	!Policy ^ rules ^ all_rules := [R | !.Policy ^ rules ^ all_rules].

filter_map_uas_set(Test, Policy, OutValues, !IO) :-
	UAs = get_uas(Policy ^ rules, !.IO, !:IO),
	list.foldl(
		pred(UA::in, !.Values::in, !:Values::out) is det :-
		(
			if Test(UA, Value) then
				set.insert(Value, !Values)
			else true
		),
		UAs,
		set.init,
		OutValues
	).

with_pa(pa(RoleName, P, O), !Policy, !IO) :-
	with_role(RoleName, Role, !Policy, !IO),
	new_mutvar(pa(Role, P, O), Ref, !IO),
	PA = 'new pa_ref'(Ref),
	R = 'new rule_ref'(Ref),
	!Policy ^ rules ^ pa_rules := [PA | !.Policy ^ rules ^ pa_rules],
	!Policy ^ rules ^ all_rules := [R | !.Policy ^ rules ^ all_rules].
