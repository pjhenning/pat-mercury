:- module policy.flatten.

:- interface.

:- pred flatten_negative_preconds(
	list(ca_ref)::in,
	list(rh)::in,
	io::di,
	io::uo
) is det.

:- pred flatten_positive_preconds(
	list(rh)::in,
	policy_dynamic::in,
	policy_dynamic::out,
	io::di,
	io::uo
) is det.

:- pred flatten_pp(
	list(rh)::in,
	ca_ref::in,
	policy_dynamic::in,
	policy_dynamic::out,
	io::di,
	io::uo
) is det.

:- pred handle_pos_conjunct(
	role_dynamic::in,
	role_dynamic::in,
	set(role_dynamic)::in,
	set(role_dynamic)::in,
	policy_dynamic::in,
	policy_dynamic::out,
	io::di,
	io::uo
) is det.

:- pred flatten_admins(
	list(rh)::in,
	policy_dynamic::in,
	policy_dynamic::out,
	io::di, io::uo
) is det.

:- pred admin_flatten_cr(
	list(rh)::in,
	cr_ref::in,
	policy_dynamic::in,
	policy_dynamic::out,
	io::di,
	io::uo
) is det.

:- pred flatten_smers(
	list(rh)::in,
	policy_dynamic::in,
	policy_dynamic::out,
	io::di,
	io::uo
) is det.

:- pred flatten_uas(
	list(rh)::in,
	policy_dynamic::in,
	policy_dynamic::out,
	io::di,
	io::uo
) is det.

:- pred flatten_pas(
	list(rh)::in,
	policy_dynamic::in,
	policy_dynamic::out,
	io::di,
	io::uo
) is det.

:- implementation.

:- import_module policy.utils.

flatten_negative_preconds(CAs, Hierarchy, !IO) :-
	list.foldl(
		pred(CA_Ref::in, !.IO2::di, !:IO2::uo) is det :-
		(
			ca_ref(Ref) = CA_Ref,
			get_mutvar(Ref, CA_Val, !IO2),
			ca(A, BoolOrPreconds, R) = as_ca(CA_Val),
			(
				if BoolOrPreconds = preconds(PC) then
					set.filter_map(
						pred(Role::in, NegRole::out) is semidet :- neg(NegRole) = Role, 
						PC,
						Neg
					),
					set.foldl(
						pred(NR::in, !.OutSet::in, !:OutSet::out) is det :-
						(
							Sens = seniors(NR, Hierarchy),
							list.foldl(
								set.insert,
								Sens,
								!OutSet
							)
						),
						Neg, 
						set.init, 
						NegSens
					),
					set.foldl(
						pred(S::in, in, out) is det --> set.insert(neg(S)), 
						NegSens,
						PC,
						PC_Next
					),
					set_ca(CA_Ref, ca(A, preconds(PC_Next), R), !IO2),
					set.foldl(
						pred(PC_Role::in, I::di, O::uo) is det :-
						(
							PC_Role = pos(RV),
							with_occurrence(RV, pos_precond(CA_Ref), I, O)
						;
							PC_Role = neg(RV),
							with_occurrence(RV, neg_precond(CA_Ref), I, O)
						),
						PC_Next,
						!IO2
					)
				else true
			)
		), 
		CAs,
		!IO
	).

flatten_positive_preconds(Hierarchy, !Policy, !IO) :-
	list.foldl2(flatten_pp(Hierarchy), !.Policy ^ rules ^ ca_rules, !Policy, !IO).

flatten_pp(Hierarchy, CA_Ref, !Policy, !IO) :-
	ca_ref(Ref) = CA_Ref,
	get_mutvar(Ref, CA, !IO),
	ca(Admin, PC, Target) = as_ca(CA),
	(
		preconds(PCR) = PC,
		list.filter_map(
			pred(PC_Role::in, PosRole::out) is semidet :- pos(PosRole) = PC_Role,
			set.to_sorted_list(PCR),
			PosRoles,
			Neg
		),
		list.filter_map(
			pred(N::in, NegRole::out) is semidet :- neg(NegRole) = N,
			Neg,
			NegRoles
		),
		list.foldl(
			pred(PosRole::in, InList::in, OutList::out) is det :-
			(
				Seniors = seniors(PosRole, Hierarchy),
				OutList = [Seniors | InList]
			),
			PosRoles,
			[],
			SeniorsOfRoles
		),
		PosConjunct = cartesian_product(SeniorsOfRoles),
		list.foldl2(
			handle_pos_conjunct(Admin, Target, set.from_list(NegRoles)), 
			PosConjunct,
			!Policy,
			!IO
		)
	;
		bool(_) = PC,
		!:Policy = !.Policy,
		!:IO = !.IO
	).

handle_pos_conjunct(Admin, Target, NegRoles, CS, !Policy, !IO) :-
	if utils.disjoint(NegRoles, CS) then
		set.map(pred(R::in, V::out) is det :- V = neg(R), NegRoles, NegVals),
		set.map(pred(R::in, V::out) is det :- V = pos(R), CS, CS_Vals),
		NewPreconds = set.union(NegVals, CS_Vals),
		preconds_dyn_to_static(NewPreconds, PrecondsStatic),
		NewCA = ca(Admin ^ name, preconds(PrecondsStatic), Target ^ name), 
		with_ca(NewCA, !Policy, !IO)
	else true.

:- pred admin_flatten_ca(
	list(rh)::in,
	ca_ref::in,
	policy_dynamic::in,
	policy_dynamic::out,
	io::di,
	io::uo
) is det.
admin_flatten_ca(Hierarchy, CA_Ref, !Policy, !IO) :-
	ca_ref(Ref) = CA_Ref,
	get_mutvar(Ref, CA, !IO),
	ca(Admin, BoP, Target) = as_ca(CA),
	(
		BoP = bool(B),
		PC = bool(B)
	;
		BoP = preconds(DP),
		preconds_dyn_to_static(DP, PrecondsStatic),
		PC = preconds(PrecondsStatic)
	),
	Seniors = seniors(Admin, Hierarchy),
	list.foldl2(
		pred(Senior::in, PI::in, PO::out, di, uo) is det -->
			with_ca(ca(Senior ^ name, PC, Target ^ name), PI, PO),
		Seniors,
		!Policy,
		!IO
	).

flatten_admins(Hierarchy, !Policy, !IO) :-
	list.foldl2(admin_flatten_ca(Hierarchy), !.Policy ^ rules ^ ca_rules, !Policy, !IO),
	list.foldl2(admin_flatten_cr(Hierarchy), !.Policy ^ rules ^ cr_rules, !Policy, !IO).

admin_flatten_cr(Hierarchy, CR_Ref, !Policy, !IO) :-
	cr_ref(Ref) = CR_Ref,
	get_mutvar(Ref, CR, !IO),
	cr(AdminOrBool, Target) = as_cr(CR),
	(
		if (role(Admin) = AdminOrBool) then
			Seniors = seniors(Admin, Hierarchy),
			list.foldl2(
				pred(Senior::in, PI::in, PO::out, di, uo) is det -->
					with_cr(cr(role(Senior ^ name), Target ^ name), PI, PO),
				Seniors,
				!Policy,
				!IO
			)
		else true
	).

flatten_smers(Hierarchy, !Policy, !IO) :-
	SMERs = get_smers(!.Policy ^ rules, !.IO, !:IO),
	list.foldl2(
		pred(CurrentSMER::in, !.Pol::in, !:Pol::out, I::di, O::uo) is det :-
		(
			solutions(
				pred(SeniorSMER::out) is nondet :- 
					senior_smer(CurrentSMER, Hierarchy, SeniorSMER), 
				SeniorSMERs
			),
			list.foldl2(
				pred(smer(A, B)::in, !.P::in, !:P::out, di, uo) is det -->
					with_smer(smer(A ^ name, B ^ name), !P),
				SeniorSMERs,
				!Pol,
				I,
				O
			)
		),
		SMERs,
		!Policy,
		!IO
	).

flatten_uas(Hierarchy, !Policy, !IO) :- 
	UAs = get_uas(!.Policy ^ rules, !.IO, !:IO),
	list.foldl2(
		pred(ua(U, R)::in, !.Pol::in, !:Pol::out, I::di, O::uo) is det :-
		(
			Juniors = juniors(R, Hierarchy),
			list.foldl2(
				pred(Junior::in, !.P::in, !:P::out, di, uo) is det -->
					with_ua(ua(U, Junior ^ name), !P), 
				Juniors,
				!Pol,
				I,
				O
			)
		),
		UAs,
		!Policy,
		!IO
	).

flatten_pas(Hierarchy, !Policy, !IO) :- 
	PAs = get_pas(!.Policy ^ rules, !.IO, !:IO),
	list.foldl2(
		pred(pa(R, Perm, Obj)::in, !.Pol::in, !:Pol::out, I::di, O::uo) is det :-
		(
			Seniors = seniors(R, Hierarchy),
			list.foldl2(
				pred(Senior::in, !.P::in, !:P::out, di, uo) is det -->
					with_pa(pa(Senior ^ name, Perm, Obj), !P), 
				Seniors,
				!Pol,
				I,
				O
			)
		),
		PAs,
		!Policy, 
		!IO
	).


%% Flattening utils

:- func cartesian_product(list(list(T))) = list(set(T)).
cartesian_product(Lists) = Products :-
	cart_rec(set.init, Lists, [], Products).

:- pred cart_rec(
	set(T)::in,
	list(list(T))::in,
	list(set(T))::in,
	list(set(T))::out
) is det.
% TODO: Ensure that this is TCO-friendly
cart_rec(Trail, To_Traverse, !Products) :-
	(
		To_Traverse = [],
		!:Products = [Trail | !.Products]
	;
		To_Traverse = [Current | Rest],
		list.foldl(
			pred(E::in, in, out) is det --> 
				cart_rec(set.insert(Trail, E), Rest),
			Current,
			!Products
		)
	).

