:- module policy.stats.

:- interface.

:- func get_stats(policy_dynamic::in, io::di, io::uo) = (policy_stats::out) is det.


:- implementation.

get_stats(Policy, !IO) = Stats :-
	% TODO: Count occurrences in goal as well!
	AdminCount = get_admin_count(Policy, !.IO, !:IO),
	PosCount = get_pos_count(Policy, !.IO, !:IO),
	NegCount = get_neg_count(Policy, !.IO, !:IO),
	MixedCount = get_mixed_count(Policy, !.IO, !:IO),
	IrrevCount = get_irrev_count(Policy, !.IO, !:IO),
	Stats = policy_stats(
		AdminCount,
		PosCount,
		NegCount,
		MixedCount,
		IrrevCount,
		list.length(Policy ^ roles)
	).

:- func get_admin_count(policy_dynamic::in, io::di, io::uo) = (int::out) is det.
get_admin_count(Policy, !IO) = set.count(Admins) :-
	filter_roles_by_occurrences(
		pred(Occurrences::in) is semidet :-
		(
			list.member(Occurrence, Occurrences),
			(
				Occurrence = ca_admin(_)
			;
				Occurrence = cr_admin(_)
			)
		),
		Policy ^ roles,
		Admins,
		!IO
	).

:- func get_pos_count(policy_dynamic::in, io::di, io::uo) = (int::out) is det.
get_pos_count(Policy, !IO) = set.count(Pos) :-
	filter_roles_by_occurrences(
		pred(Occurrences::in) is semidet :-
		(
			list.member(Occurrence, Occurrences),
			Occurrence = pos_precond(_)
		),
		Policy ^ roles,
		Pos,
		!IO
	).

:- func get_neg_count(policy_dynamic::in, io::di, io::uo) = (int::out) is det.
get_neg_count(Policy, !IO) = set.count(Neg) :-
	filter_roles_by_occurrences(
		pred(Occurrences::in) is semidet :-
		(
			list.member(Occurrence, Occurrences),
			Occurrence = neg_precond(_)
		),
		Policy ^ roles,
		Neg,
		!IO
	).

:- func get_mixed_count(policy_dynamic::in, io::di, io::uo) = (int::out) is det.
get_mixed_count(Policy, !IO) = set.count(Mixed) :-
	filter_roles_by_occurrences(
		pred(Occurrences::in) is semidet :-
		(
			list.member(Occurrence1, Occurrences),
			Occurrence1 = pos_precond(_),
			list.member(Occurrence2, Occurrences),
			Occurrence2 = neg_precond(_)
		),
		Policy ^ roles,
		Mixed,
		!IO
	).

:- func get_irrev_count(policy_dynamic::in, io::di, io::uo) = (int::out) is det.
get_irrev_count(Policy, !IO) = set.count(Irrev) :-
	filter_roles_by_occurrences_rules(
		pred(OccurrencesRules::in) is semidet :-
		(
			list.member({Occurrence, Rule}, OccurrencesRules),
			Occurrence = cr_admin(_),
			Rule = cr(bool(f), _)
		),
		Policy ^ roles,
		Irrev,
		!IO
	).