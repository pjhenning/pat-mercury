:- module policy.slice.

:- interface.

:- type relpos.
:- type relneg.

:- pred slice_prep(
	policy_dynamic::in,
	goal::in,
	relpos::out,
	relneg::out,
	io::di,
	io::uo
) is det. 

:- pred slice(
	relpos::in,
	relneg::in,
	policy_dynamic::in,
	policy_dynamic::out,
	io::di,
	io::uo
) is det.


:- implementation.

:- type relpos == list(role_dynamic).
:- type relneg == list(role_dynamic).

% TODO: may be appropriate to eliminate some smer rules during slicing

slice_prep(Policy, Goal, RelPos, RelNeg, !IO) :-
	CAs = get_cas(Policy ^ rules, !.IO, !:IO),
	set.foldl(
		pred(G_Role::in, !.Roles::in, !:Roles::out) is det :-
		(
			relevant_roles(G_Role, CAs, RR),
			list.append(RR, !Roles)
		),
		Goal ^ g_roles,
		[],
		RelRoles
	),
	list.filter(pred(pos(_)::in) is semidet, RelRoles, RelPosPrePre, RelNegPre),
	list.filter_map(pred(pos(R)::in, R::out) is semidet, RelPosPrePre, RelPosPre),
	set.foldl(
		pred(G_Role::in, !.Pos::in, !:Pos::out) is det :-
			!:Pos = [G_Role | !.Pos],
		Goal ^ g_roles,
		RelPosPre,
		RelPos
	),
	list.filter_map(pred(neg(R)::in, R::out) is semidet, RelNegPre, RelNeg).

slice(RelPos, RelNeg, !Policy, !IO) :-
	write_string("\nRelPos:\n", !IO),
	write_doc(format(RelPos), !IO),
	write_string("\nRelNeg:\n", !IO),
	write_doc(format(RelNeg), !IO),
	write_string("\n", !IO),
	SliceRoles =
		set.difference(
			set.list_to_set(!.Policy ^ roles),
			set.union(list_to_set(RelPos), list_to_set(RelNeg))
		),
	write_string("SliceRoles (to remove):\n", !IO),
	write_doc(format(SliceRoles), !IO),
	write_string("\n", !IO),
	without_roles(SliceRoles, !Policy, !IO),
	filter_cas(
		pred(ca(_, _, Target)::in) is semidet :-
			not list.member(Target, RelPos),
		!.Policy ^ rules ^ ca_rules,
		SliceRules1,
		!IO
	),
	without_ca_rules(SliceRules1, !Policy, !IO),
	filter_crs(
		% may need to backchain ca rules that allow membership of admin
		pred(cr(_, Target)::in) is semidet :-
			not list.member(Target, RelNeg),
		!.Policy ^ rules ^ cr_rules,
		SliceRules2,
		!IO
	),
	without_cr_rules(SliceRules2, !Policy, !IO).

:- pred relevant_roles(role_dynamic::in, list(ca)::in, list(precond_role(role_dynamic))::out) is det.
relevant_roles(StartRole, CAs, Roles) :-
	solutions(relevant_role(StartRole, CAs),	RolesTop),
	list.foldl(pred(TopRole::in, !.Roles::in, !:Roles::out) is det :-
		(
			(
				pos(TR) = TopRole
			;
				neg(TR) = TopRole
			),
			relevant_roles(TR, CAs, NextRoles),
			list.append(NextRoles, !Roles)
		),
		RolesTop,
		[],
		RolesNext
	),
	list.append(RolesTop, RolesNext, Roles).

:- pred relevant_role(
		role_dynamic::in,
		list(ca)::in,
		precond_role(role_dynamic)::out
	) is nondet.
relevant_role(Role, CAs, RelevantRole) :-
	list.member(ca(Admin, preconds(PC), Role), CAs),
	(
		set.member(RelevantRole, PC)
	;
		RelevantRole = pos(Admin)
	).