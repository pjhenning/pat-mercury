:- module policy.

:- interface.

:- include_module
	parse, rules, roles, flatten, hierarchy, conflicts,
	slice, stats, graph, reachability, utils.

:- import_module policy.graph.

:- import_module string.
:- import_module list.
:- import_module array.
:- import_module set.
:- import_module store.
:- import_module io.
:- import_module maybe.

%% Public types

:- type goal.

:- type policy_dynamic --->
	policy_dynamic(
		rules :: rules_dynamic,
		roles :: list(role_dynamic),
		users :: set(policy_user)
	).

:- type rules_dynamic --->
	rules_dynamic(
		all_rules   :: list(rule_ref),
		ca_rules    :: list(ca_ref),
		cr_rules    :: list(cr_ref),
		rh_rules    :: list(rh_ref),
		smer_rules  :: list(smer_ref),
		ua_rules    :: list(ua_ref),
		pa_rules    :: list(pa_ref)
	).
:- type rule_ref.
:- type ca_ref.
:- type cr_ref.
:- type rh_ref.
:- type smer_ref.
:- type ua_ref.
:- type pa_ref.

:- type role_dynamic --->
	role_dynamic(
		name        :: string,
		occurrences :: io_mutvar(list(role_occurrence))
	).

:- type precond_role(Role_T) --->
	pos(Role_T);
	neg(Role_T).

:- type role_occurrence.
:- type rule_static.
:- type policy_user.

%% Public functions and predicates

:- func parse(string::in, io::di, io::uo) = (list(rule_static)::out) is det.

:- func init(list(rule_static)::in, io::di, io::uo) = (policy_dynamic::out) is det.

:- pred log_rules(policy_dynamic::in, io::di, io::uo) is det.

:- func get_role_names(policy_dynamic::in) = (array(string)::out) is det.

:- func get_user_names(policy_dynamic::in) = (array(string)::out) is det.

:- func flatten(policy_dynamic::in, io::di, io::uo) = (policy_dynamic::out) is det.

:- func goal(policy_user::in, array(string)::in, policy_dynamic::in, io::di, io::uo) =
	(goal::out) is det.

:- func slice(policy_dynamic::in, goal::in, io::di, io::uo) =
	(policy_dynamic::out) is det.

:- func make_stats_report(policy_dynamic::in, io::di, io::uo) = (string::out) is det.

:- func get_reduced_graph(policy_dynamic::in, goal::in, io::di, io::uo) =
	(list(graph.transition)::out) is det.

:- func build_dot_graph(policy_dynamic::in) = (string::out) is det.

:- func check_reachability(
	goal::in,
	list(graph.transition)::in,
	policy_dynamic::in,
	io::di,
	io::uo
) = (maybe(array(array(string)))::out) is det.


:- implementation.

:- import_module policy.parse.
:- import_module policy.rules.
:- import_module policy.roles.
:- import_module policy.hierarchy.
:- import_module policy.flatten.
:- import_module policy.conflicts.
:- import_module policy.slice.
:- import_module policy.stats.
:- import_module policy.reachability.

:- import_module int.
:- import_module solutions.
:- import_module exception.
:- import_module pretty_printer.

:- type goal --->
	goal(
		target :: policy_user,
		g_roles :: set(role_dynamic)
	).


%% Base policy types

:- type rule_ref --->
	some [T] (rule_ref(io_mutvar(T)) => rule_interface(T)).

:- type ca_ref --->
	some [T] (ca_ref(io_mutvar(T)) => ca_interface(T)).

:- type ca_exist --->
	some [T] (ca_exist(T) => ca_interface(T)).

:- type cr_ref --->
	some [T] (cr_ref(io_mutvar(T)) => cr_interface(T)).

:- type rh_ref --->
	some [T] (rh_ref(io_mutvar(T)) => rh_interface(T)).

:- type smer_ref --->
	some [T] (smer_ref(io_mutvar(T)) => smer_interface(T)).

:- type ua_ref --->
	some [T] (ua_ref(io_mutvar(T)) => ua_interface(T)).

:- type pa_ref --->
	some [T] (pa_ref(io_mutvar(T)) => pa_interface(T)).

:- type rule_static == rule(role_static).
:- type rule_dynamic == rule(role_dynamic).

:- type rule(Role_T) ---> 
	ca(Role_T, bool_or_preconds(Role_T), Role_T);
	cr(bool_or_role(Role_T), Role_T);
	rh(Role_T, Role_T);
	smer(Role_T, Role_T);
	ua(policy_user, Role_T);
	pa(Role_T, policy_permission, permission_object).

:- type bool_or_role(Role_T) --->
	bool(boolean);
	role(Role_T).

:- type bool_or_preconds(Role_T) --->
	bool(boolean);
	preconds(set(precond_role(Role_T))).

:- type boolean --->
	t;
	f.

:- type ca ---> 
	ca(role_dynamic, bool_or_preconds(role_dynamic), role_dynamic).

:- type ca_static --->
	ca(role_static, bool_or_preconds(role_static), role_static).

:- type cr --->
	cr(bool_or_role(role_dynamic), role_dynamic).

:- type cr_static --->
	cr(bool_or_role(role_static), role_static).

:- type rh --->
	rh(role_dynamic, role_dynamic).

:- type rh_static --->
	rh(role_static, role_static).

:- type smer --->
	smer(role_dynamic, role_dynamic).

:- type smer_static --->
	smer(role_static, role_static).

:- type ua --->
	ua(policy_user, role_dynamic).

:- type ua_static --->
	ua(policy_user, role_static).

:- type pa --->
	pa(role_dynamic, policy_permission, permission_object).

:- type pa_static --->
	pa(role_static, policy_permission, permission_object).

:- typeclass rule_interface(T) where [
	func as_rule(T::in) = (rule_dynamic::out) is det
].
:- instance rule_interface(ca) where [ as_rule(ca(A, B, C)) = ca(A, B, C) ].
:- instance rule_interface(cr) where [ as_rule(cr(A, B)) = cr(A, B) ].
:- instance rule_interface(rh) where[ as_rule(rh(A, B)) = rh(A, B) ].
:- instance rule_interface(smer) where [ as_rule(smer(A, B)) = smer(A, B) ].
:- instance rule_interface(ua) where [ as_rule(ua(A, B)) = ua(A, B) ].
:- instance rule_interface(pa) where [ as_rule(pa(A, B, C)) = pa(A, B, C) ].

:- typeclass ca_interface(T) <= rule_interface(T) where [
	func as_ca(T::in) = (ca::out) is det
].
:- instance ca_interface(ca) where [ as_ca(CA) = CA ].

:- typeclass cr_interface(T) <= rule_interface(T) where [
	func as_cr(T::in) = (cr::out) is det
].
:- instance cr_interface(cr) where [ as_cr(CR) = CR ].

:- typeclass rh_interface(T) <= rule_interface(T) where [
	func as_rh(T::in) = (rh::out) is det
].
:- instance rh_interface(rh) where [ as_rh(RH) = RH ].

:- typeclass smer_interface(T) <= rule_interface(T) where [
	func as_smer(T::in) = (smer::out) is det
].
:- instance smer_interface(smer) where [ as_smer(SMER) = SMER ].

:- typeclass ua_interface(T) <= rule_interface(T) where [
	func as_ua(T::in) = (ua::out) is det
].
:- instance ua_interface(ua) where [ as_ua(UA) = UA ].

:- typeclass pa_interface(T) <= rule_interface(T) where [
	func as_pa(T::in) = (pa::out) is det
].
:- instance pa_interface(pa) where [ as_pa(PA) = PA ].

:- type policy_user == string.
:- type policy_permission == string.
:- type permission_object == string.

:- type role_static == string.

:- type role_occurrence --->
	ca_admin(ca_ref);
	pos_precond(ca_ref);
	neg_precond(ca_ref);
	ca_target(ca_ref);
	cr_admin(cr_ref);
	cr_target(cr_ref);
	rh_senior(rh_ref);
	rh_junior(rh_ref).

:- type role_occurrence_static --->
	ca_admin(ca);
	pos_precond(ca);
	neg_precond(ca);
	ca_target(ca);
	cr_admin(cr);
	cr_target(cr);
	rh_senior(rh);
	rh_junior(rh).

:- type role_cat --->
	admin;
	positive;
	negative;
	irrevocable.

:- type policy_stats --->
	policy_stats(
		admin_count :: int,
		pos_count   :: int,
		neg_count   :: int,
		mixed_count :: int,
		irrev_count :: int,
		total_count :: int
	).


%% Parsing

:- pragma foreign_export("C#", parse(in, di, uo) = out, "parse").
parse(PolicyStr, !IO) = Rules :-
	policy.parse.parse_rules_string(PolicyStr, Rules).


%% User utils

:- pragma foreign_export("C#", get_user_names(in) = out, "get_user_names").
get_user_names(Policy) = array(NamesList) :-
	set.to_sorted_list(Policy ^ users, NamesList).

:- pred with_user(policy_user::in, policy_dynamic::in, policy_dynamic::out) is det.
with_user(User, !Policy) :-
	!Policy ^ users := set.insert(!.Policy ^ users, User).


%% Policy processing

:- pragma foreign_export("C#", init(in, di, uo) = out, "init").
init(InRules, !IO) = Policy :-
	Rules_Init = rules_dynamic([], [], [], [], [], [], []),
	Policy_Init = policy_dynamic(Rules_Init, [], set.init),
	list.foldl2(
		collect_rule_data, 
		InRules, 
		Policy_Init, 
		Policy, 
		!IO
	).


%% Rule utils

:- pragma foreign_export("C#", log_rules(in, di, uo), "log_rules").
log_rules(Policy, !IO) :-
	StaticRules = policy.rules.get_all_static(Policy ^ rules, !.IO, !:IO),
	write_doc(format(StaticRules), !IO).


%% Role utils

:- pragma foreign_export("C#", get_role_names(in) = out, "get_role_names").
get_role_names(Policy) = array(NamesList) :-
	list.map(pred(role_dynamic(Name, _)::in, Name::out) is det, Policy ^ roles, NamesList).


%% Role statistics

:- pragma foreign_export("C#", make_stats_report(in, di, uo) = out, "make_stats_report").
make_stats_report(Policy, !IO) = Report :-
	Stats = get_stats(Policy, !.IO, !:IO),
	Admin = "Admin roles: " ++ string.from_int(Stats ^ admin_count) ++ "\n",
	Positive = "Positive roles: " ++ string.from_int(Stats ^ pos_count) ++ "\n",
	Negative = "Negative roles: " ++ string.from_int(Stats ^ neg_count) ++ "\n",
	Mixed = "Mixed roles: " ++ string.from_int(Stats ^ mixed_count) ++ "\n",
	Irrevocable = "Irrevocable roles: " ++ string.from_int(Stats ^ irrev_count) ++ "\n",
	Total = "Total roles: " ++ string.from_int(Stats ^ total_count) ++ "\n",
	Report = Admin ++ Positive ++ Negative ++ Mixed ++ Irrevocable ++ Total.


%% Flattening

:- pragma foreign_export("C#", flatten(in, di, uo) = out, "flatten").
flatten(!.Policy, !IO) = !:Policy :-
	Hierarchy = get_rhs(!.Policy ^ rules, !.IO, !:IO), 
	CA_Rules = !.Policy ^ rules ^ ca_rules,
	flatten_negative_preconds(CA_Rules, Hierarchy, !IO),
	flatten_positive_preconds(Hierarchy, !Policy, !IO),
	flatten_admins(Hierarchy, !Policy, !IO),
	flatten_smers(Hierarchy, !Policy, !IO),
	flatten_uas(Hierarchy, !Policy, !IO),
	flatten_pas(Hierarchy, !Policy, !IO).


%% Goal util

:- pragma foreign_export("C#", goal(in, in, in, di, uo) = out, "goal").
goal(User, RolesArray, Policy, !IO) = goal(User, Roles) :-
	array.foldl2(
		pred(RoleStatic::in, !.RoleSet::in, !:RoleSet::out, !.IO2::di, !:IO2::uo) is det :-
		(
			list.take_while(
				pred(R::in) is semidet :-
					R \= role_dynamic(RoleStatic, _),
				Policy ^ roles,
				_,
				Rem
			),
			(
				if Rem = [RD | _] then
					set.insert(RD, !RoleSet)
				else true
			)
		),
		RolesArray,
		set.init,
		Roles,
		!IO
	).

%% Slicing

:- pragma foreign_export("C#", slice(in, in, di, uo) = out, "slice").
slice(!.Policy, Goal, !IO) = !:Policy :-
	write_string("Roles before slicing:\n", !IO),
	write_doc(format(!.Policy ^ roles), !IO),
	slice.slice_prep(!.Policy, Goal, RelPos, RelNeg, !IO),
	write_string("\nRoles after slice prep:\n", !IO),
	write_doc(format(!.Policy ^ roles), !IO),
	slice.slice(RelPos, RelNeg, !Policy, !IO),
	write_string("\nRoles after slicing:\n", !IO),
	write_doc(format(!.Policy ^ roles), !IO),
	write_string("\n", !IO).


%% Policy state graph

:- pragma foreign_export(
	"C#", get_reduced_graph(in, in, di, uo) = out, "get_reduced_graph"
).
get_reduced_graph(Policy, Goal, !IO) = Transitions :-
	pretty_printer.get_default_params(FormatParams, !IO),
	NewParams = FormatParams ^ pp_max_lines := 1000,
	pretty_printer.set_default_params(NewParams, !IO),
	write_string("Graph init...", !IO),
	graph.init(Policy, Edges, !IO),
	write_string("Done :)\n", !IO),
	write_string("Graph reduction...\n", !IO),
	graph.reduce(Policy, Goal, Edges, Transitions, !IO),
	write_string("Reduced graph has the following transitions: ", !IO),
	write_doc(format(Transitions), !IO),
	write_string("\n", !IO).

/*
:- pred without_role(policy_role::in, policy::in, policy::out) is det.
without_role(Role, !Policy) :- 
	Old_Roles = !.Policy ^ roles,
	set.delete(Role, Old_Roles, New_Roles),
	!:Policy = !.Policy ^ roles := New_Roles,
	set.filter(
		Role_Not_Present, 
		!.Policy ^ rules,
		New_Rules
	),
	!:Policy = !.Policy ^ rules := New_Rules,
	Role_Not_Present = 
	(
		pred(Rule::in) is semidet :- 
		Rule \= ca(Role, _, _),
		Rule \= ca(_, _, Role),
		Rule = ca(_, preconds(Precond_Roles), _),
		list.all_false(PC_Has_Role, Precond_Roles)
	),
	PC_Has_Role = 
	(
		pred(Precond_Role::in) is semidet :- 
		(
			Precond_Role = pos(U_Role),
			U_Role = Role
		;
			Precond_Role = neg(U_Role),
			U_Role = Role
		)
	).
*/

% DOT output example:
%	digraph G {
%		1 [label="r1"]
%		2 [label="r1, r2"]
%		3 [label="r1, r2, r3"]
%		1->2 [label="  +r2"]
%		2->3 [label="  +r3"]
%	}

build_dot_graph(_) = Dot_Graph :-
	Dot_Graph = "digraph G {\n" ++ Nodes_Section ++ "\n" ++ Edge_Section ++ "\n}",
	Nodes_Section = "",
	Edge_Section = "".
/*
:- func render_edges(list(pol_edge)::in) = (string::out) is det.
render_edges(Edges) = Edge_Section :-
	Edge_Section = string.join_list("\n", Lines),
	list.map(
		pred(pol_edge(Action, From, To, _By, _Rule)::in, Line::out) is det :-
			(
				assign(R) = Action,
				Line = "\t" ++ From ^ n_id ++ "->" ++ To ^ n_id ++ " [label=\" +" ++ R ++ "\"]"
			;
				revoke(R) = Action,
				Line = "\t" ++ From ^ n_id ++ "->" ++ To ^ n_id ++ " [label=\" -" ++ R ++ "\"]"
			), 
		Edges, 
		Lines
	).

:- func render_nodes(list(pol_node)::in) = (string::out) is det.
render_nodes(Nodes) = Nodes_Section :- 
	Nodes_Section = string.join_list("\n", Lines),
	list.map(
		pred(Node::in, Line::out) is det :- (
			Roles_Str = string.join_list(", ", Roles_L),
			Roles_L = set.to_sorted_list(Node ^ n_roles),
			Line = "\t" ++ Node ^ n_id ++ " [label=\"" ++ Roles_Str ++ "\"]"
		), 
		Nodes, 
		Lines
	).
*/


%% Reachability Analysis

:- pragma foreign_export(
	"C#", check_reachability(in, in, in, di, uo) = out, "check_reachability"
).
check_reachability(Goal, Transitions, Policy, !IO) = Report :-
	write_string("Reachability time :)\n", !IO),
	reachable(Goal, Transitions, Policy, Conclusion, !IO),
	reachability_report(Conclusion, Report),
	write_string("Report:\n", !IO),
	write_doc(format(Report), !IO),
	write_string("\n", !IO).

:- pred reachability_report(
	maybe(list({list(transition), list(list(list(policy_user)))}))::in,
	maybe(array(array((string))))::out
) is det.
reachability_report(Conclusion, Report) :-
	(
		Conclusion = no,
		Report = no
	;
		Conclusion = yes(ExecPaths),
		% Report per path
		list.map(
			pred({Transitions, UsersLists}::in, PathSteps::out) is det :-
			(
				% Report per transition
				list.foldl_corresponding(
					pred(
						transition(_, _, Edges)::in,
						UsersList::in,
						!.StepsPart::in,
						!:StepsPart::out
					) is det :-
					(
						% Report per edge
						list.map_corresponding(
							pred(pol_edge(Action, _, _)::in, Users::in, Step::out) is det :-
							(
								list.map(
									pred(User::in, UserQ::out) is det :-
										UserQ = "'" ++ User ++ "'",
									Users,
									UsersQ
								),
								AdminsStr = string.join_list(" OR ", UsersQ),
								(
									Action = assign(ca(_, _, role_dynamic(R, _))),
									ActionStr = "assigns"
								;
									Action = revoke(cr(_, role_dynamic(R, _))),
									ActionStr = "revokes"
								),
								Step =
									"User " ++ AdminsStr ++ " " ++ ActionStr ++ " role \'" ++ R ++ "\'"
							),
							Edges,
							UsersList,
							StepsPartPart
						),
						list.append(!.StepsPart, StepsPartPart, !:StepsPart)
					),
					Transitions,
					UsersLists,
					[],
					Steps
				),
				array.from_list(Steps, PathSteps)
			),
			ExecPaths,
			PathsStepsList
		),
		array.from_list(PathsStepsList, PathsSteps),
		Report = yes(PathsSteps)
	).

