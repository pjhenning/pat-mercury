:- module policy.graph.

:- interface.

:- type pol_edge --->
	pol_edge(
		action :: action,
		from   :: pol_node,
		to     :: pol_node
	).

:- type action --->
	assign(ca);
	revoke(cr).

:- type pol_node --->
	pol_node(
		n_id    :: string,
		n_roles :: set(role_dynamic)
	).

:- type transition --->
	transition(
		t_from  :: pol_node,
		t_to    :: pol_node,
		edges :: list(pol_edge)
	).

:- pred init(policy_dynamic::in, list(pol_edge)::out, io::di, io::uo) is det.
:- pred reduce(
	policy_dynamic::in,
	goal::in,
	list(pol_edge)::in,
	list(transition)::out,
	io::di,
	io::uo
) is det.

:- pred admins_for_edge(
	pol_edge::in,
	policy_dynamic::in,
	maybe(list(policy_user))::out,
	io::di,
	io::uo
) is det.

:- implementation.

:- import_module policy.roles.
:- import_module policy.utils.

%% TODO: decide whether you still need this type
/*
:- type policy_graph
	---> policy_graph(
		edges :: list(pol_edge),
		nodes :: list(pol_node)
	).
*/

init(Policy, Edges, !IO) :-
	write_string("Roles:\n", !IO),
	write_doc(format(Policy ^ roles), !IO),
	write_string("\n", !IO),
	% Get power set of roles for node pool
	NodesPool = power_set(Policy ^ roles),
	write_string("NodesPool:\n", !IO),
	write_doc(format(NodesPool), !IO),
	write_string("\n", !IO),
	% Iterate through ca rules to build a list of edges
	list.foldl4(
		process_ca_graph_data(NodesPool),
		Policy ^ rules ^ ca_rules,
		[],
		CA_Edges,
		[],
		CA_Nodes,
		0,
		CA_NodeCount,
		!IO
	),
	% Iterate through cr rules to add to list of edges
	list.foldl4(
		process_cr_graph_data(NodesPool),
		Policy ^ rules ^ cr_rules,
		CA_Edges,
		Edges,
		CA_Nodes,
		_,
		CA_NodeCount,
		_,
		!IO
	)/*,
	write_string("ca and cr rules yield the following edges:\n", !IO),
	write_doc(format(Edges), !IO),
	write_string("\n", !IO)
	*/.

reduce(Policy, Goal, Edges, Transitions, !IO) :-
	roles.filter_roles_by_occurrences(
		pred(Occurrences::in) is semidet :-
		(
			list.member(Occurrence, Occurrences),
			Occurrence = pos_precond(_)
		),
		Policy ^ roles,
		P_Pos,
		P_NonPos,
		!IO
	),
	Pos: set(role_dynamic) = set.union(P_Pos, Goal ^ g_roles),
	write_string("Pos roles = \n", !IO),
	write_doc(format(Pos), !IO),
	write_string("\n", !IO),
	% TODO: make sure this handles cases where g_roles has members that 
	% P_NonPos doesn't in the way you are assuming
	NonPos: set(role_dynamic) = set.difference(P_NonPos, Goal ^ g_roles),
	write_string("NonPos roles = \n", !IO),
	write_doc(format(NonPos), !IO),
	write_string("\n", !IO),
	roles.filter_roles_by_occurrences(
		pred(Occurrences::in) is semidet :-
		(
			list.member(Occurrence, Occurrences),
			Occurrence = neg_precond(_)
		),
		Policy ^ roles,
		Neg,
		NonNeg,
		!IO
	),
	write_string("Neg roles = \n", !IO),
	write_doc(format(Neg), !IO),
	write_string("\n", !IO),
	write_string("NonNeg roles = \n", !IO),
	write_doc(format(NonNeg), !IO),
	write_string("\n", !IO),
	list.negated_filter(
		pred(Edge::in) is semidet :-
		(
			Edge = pol_edge(revoke(cr(_, R)), _, _),
			set.member(R, NonNeg)
		;
			Edge = pol_edge(assign(ca(_, _, R)), _, _),
			set.member(R, NonPos)
		),
		Edges,
		FilteredEdges
	),
	list.filter(
		pred(TestEdge::in) is semidet :-
			invisible_transition(NonNeg, NonPos, TestEdge),
		FilteredEdges,
		InvisibleEdges,
		VisibleEdges
	),
	write_string("Visible edges \n", !IO),
	write_doc(format(VisibleEdges), !IO),
	write_string("\n", !IO),
	write_string("Invisible edges \n", !IO),
	write_doc(format(InvisibleEdges), !IO),
	write_string("\n", !IO),
	collapsed_transitions(
		VisibleEdges,
		InvisibleEdges,
		CollapsedEdges,
		CollapsedTransitions
	),
	write_string("Collapsed transitions: \n", !IO),
	write_doc(format(CollapsedTransitions), !IO),
	write_string("\n", !IO),
	Invis = set.list_to_set(InvisibleEdges),
	UncollapsedEdges = set.difference(Invis, CollapsedEdges),
	consolidate_transitions(
		VisibleEdges,
		UncollapsedEdges,
		CollapsedTransitions,
		Transitions
	).

:- pred collapsed_transitions(
	list(pol_edge)::in,
	list(pol_edge)::in,
	set(pol_edge)::out,
	list(transition)::out
) is det.
collapsed_transitions(
	VisibleEdges,
	InvisibleEdges,
	CollapsedEdges,
	Transitions
) :-
	list.filter(
		pred(pol_edge(_, _, ToInvis)::in) is semidet :-
		(
			list.member(InvisEdge, InvisibleEdges),
			InvisEdge = pol_edge(_, ToInvis, _)
		),
		VisibleEdges,
		TransRoots
	),
	list.foldl2(
		pred(TransRoot::in, !.Coll::in, !:Coll::out, !.Trans::in, !:Trans::out) is det :-
		(
			transitions_of_root(TransRoot, InvisibleEdges, CollEdges, Ts),
			set.union(CollEdges, !Coll),
			list.append(Ts, !Trans)
		),
		TransRoots,
		set.init,
		CollapsedEdges,
		[],
		Transitions
	).

:- pred transitions_of_root(
	pol_edge::in,
	list(pol_edge)::in,
	set(pol_edge)::out,
	list(transition)::out
) is det.
transitions_of_root(Root, InvisibleEdges, CollapsedEdges, Transitions) :-
	collapse_rec(
		InvisibleEdges,
		Root,
		CollapsedEdges,
		FinalEdges,
		TransitionsEdges
	),
	list.map_corresponding(
		pred(FinalEdge::in, TransitionEdges::in, Transition::out) is det :-
			Transition = transition(Root ^ from, FinalEdge ^ to, TransitionEdges),
		FinalEdges,
		TransitionsEdges,
		Transitions
	).

:- pred collapse_rec(
	list(pol_edge)::in,
	pol_edge::in,
	set(pol_edge)::out,
	list(pol_edge)::out,
	list(list(pol_edge))::out
) is det.
collapse_rec(
	InvisibleEdges,
	StartEdge,
	CollapsedEdges,
	FinalEdges,
	TransitionsEdges
) :-
	CollapsedPre = make_singleton_set(StartEdge),
	solutions(next_invisible(InvisibleEdges, StartEdge), NextEdges),
	(
	if length(NextEdges) > 0 then
		list.foldl3(
			pred(
				NextEdge::in,
				!.CollEdges::in,
				!:CollEdges::out,
				!.Finals::in,
				!:Finals::out,
				!.TransEdges::in,
				!:TransEdges::out
			) is det :-
			(
				collapse_rec(InvisibleEdges, NextEdge, Coll, Fin, TrsEds),
				list.append(Fin, !Finals),
				list.foldl(
					pred(TrEds::in, !.TE::in, !:TE::out) is det :-
					(
						!:TE = [[StartEdge | TrEds] | !.TE]
					),
					TrsEds,
					!TransEdges
				),
				set.union(Coll, !CollEdges)
			),
			NextEdges,
			set.init,
			CollapsedEdges,
			[],
			FinalEdges,
			[],
			TransitionsEdges
		)
	else
		TransitionsEdges = [[StartEdge]],
		FinalEdges = [StartEdge],
		CollapsedEdges = CollapsedPre
	).

:- pred next_invisible(
	list(pol_edge)::in,
	pol_edge::in,
	pol_edge::out
) is nondet.
next_invisible(InvisibleEdges, pol_edge(_, _, NextFrom), NextEdge) :-
	list.member(NextEdge, InvisibleEdges),
	NextEdge = pol_edge(_, NextFrom, _).

:- pred invisible_transition(
	set(role_dynamic)::in,
	set(role_dynamic)::in,
	pol_edge::in
) is semidet.
invisible_transition(NonNeg, NonPos, Edge) :-
	(
		Edge = pol_edge(assign(ca(_, _, R)), _, _),
		set.member(R, NonNeg)
	;
		Edge = pol_edge(revoke(cr(_, R)), _, _),
		set.member(R, NonPos)
	).

:- pred consolidate_transitions(
	list(pol_edge)::in,
	set(pol_edge)::in,
	list(transition)::in,
	list(transition)::out
) is det.
consolidate_transitions(
	VisibleEdges,
	UncollapsedInvisibles,
	CollapsedTransitions,
	Transitions
) :-
	list.foldl(
		pred(VisibleEdge::in, Collapsed::in, TransOut::out) is det :-
		(
			% VisibleEdge is a head in collapsed: don't add it
			if collapsed(VisibleEdge, Collapsed) then
				TransOut = Collapsed
			else
				TransOut = [VisibleTrans | Collapsed],
				VisibleTrans =
					transition(VisibleEdge ^ from, VisibleEdge ^ to, [VisibleEdge]) 
		),
		VisibleEdges,
		CollapsedTransitions,
		TransitionsPre
	),
	set.foldl(
		pred(UncollapsedEdge::in, !.Trans::in, !:Trans::out) is det :-
		(
			NewTrans =
				transition(
					UncollapsedEdge ^ from, UncollapsedEdge ^ to, [UncollapsedEdge]
				),
			!:Trans = [NewTrans | !.Trans]
		),
		UncollapsedInvisibles,
		TransitionsPre,
		Transitions
	).

:- pred collapsed(pol_edge::in, list(transition)::in) is semidet.
collapsed(VisibleEdge, CollapsedTransitions) :-
	list.all_true(
		pred(transition(_, _, Edges)::in) is semidet :-
			Edges = [VisibleEdge | _],
		CollapsedTransitions
	).

:- pred process_ca_graph_data(
	set(set(role_dynamic))::in,
	ca_ref::in,
	list(pol_edge)::in,
	list(pol_edge)::out,
	list(pol_node)::in,
	list(pol_node)::out,
	int::in,
	int::out,
	io::di,
	io::uo
) is det.
process_ca_graph_data(NodesPool, CA_Ref, !Edges, !Nodes, !NodeCount, !IO) :-
	CA = get_ca(CA_Ref, !.IO, !:IO), 
	/*
	write_string("For construction of graph, procssing ca rule:\n", !IO),
	write_doc(format(CA), !IO),
	write_string("\n", !IO),
	*/
	(
		CA = ca(_, preconds(Preconds), Target),
		%write_string("committing to first branch\n", !IO),
		solutions(ca_pc_edge_pair(NodesPool, Preconds, Target), EdgePairs)
	;
		CA = ca(_, bool(t), Target),
		solutions(ca_edge_pair(NodesPool, Target), EdgePairs)
	;
		CA = ca(_, bool(f), _),
		EdgePairs = []
	),
	list.foldl3(
		process_ca_edge_pair(CA),
		EdgePairs,
		!Edges,
		!Nodes,
		!NodeCount
	)/*,
	write_string("Derived the following Edges:\n", !IO),
	write_doc(format(!.Edges), !IO),
	write_string("\n", !IO)
	*/.

:- pred process_cr_graph_data(
	set(set(role_dynamic))::in,
	cr_ref::in,
	list(pol_edge)::in,
	list(pol_edge)::out,
	list(pol_node)::in,
	list(pol_node)::out,
	int::in,
	int::out,
	io::di,
	io::uo
) is det.
process_cr_graph_data(NodesPool, CR_Ref, !Edges, !Nodes, !NodeCount, !IO) :-
	CR = get_cr(CR_Ref, !.IO, !:IO),
	/*
	write_string("For construction of graph, procssing ca rule:\n", !IO),
	write_doc(format(CR), !IO),
	write_string("\n", !IO),
	*/
	(
		(
			CR = cr(bool(t), Target)
		;
			CR = cr(role(_), Target)
		),
		solutions(cr_edge_pair(NodesPool, Target), EdgePairs)
	;
		CR = cr(bool(f), _),
		EdgePairs = []
	),
	list.foldl3(
		process_cr_edge_pair(CR),
		EdgePairs,
		!Edges,
		!Nodes,
		!NodeCount
	)/*,
	write_string("Derived the following Edges:\n", !IO),
	write_doc(format(!.Edges), !IO),
	write_string("\n", !IO)
	*/.

:- pred process_ca_edge_pair(
	ca::in,
	{set(role_dynamic), set(role_dynamic)}::in,
	list(pol_edge)::in,
	list(pol_edge)::out,
	list(pol_node)::in,
	list(pol_node)::out,
	int::in,
	int::out
) is det.
process_ca_edge_pair(CA, {FromSet, ToSet}, !Edges, !Nodes, !NodeCount) :-
	!:Edges = [Edge | !.Edges],
	Edge = pol_edge(assign(CA), From, To),
	FromExists = (pred(F::in) is semidet :-
		F = pol_node(_, FromSet)
	),
	(
	if list.find_first_match(FromExists, !.Nodes, FromRes) then
		From = FromRes
	else
		FromID = from_int(!.NodeCount + 1),
		From = pol_node(FromID, FromSet),
		!:Nodes = [From | !.Nodes],
		!:NodeCount = !.NodeCount + 1
	),
	ToExists = (pred(T::in) is semidet :-
		T = pol_node(_, ToSet)
	),
	(
	if list.find_first_match(ToExists, !.Nodes, ToRes) then
		To = ToRes
	else
		ToID = from_int(!.NodeCount + 1),
		To = pol_node(ToID, ToSet),
		!:Nodes = [To | !.Nodes],
		!:NodeCount = !.NodeCount + 1
	).

:- pred process_cr_edge_pair(
	cr::in,
	{set(role_dynamic), set(role_dynamic)}::in,
	list(pol_edge)::in,
	list(pol_edge)::out,
	list(pol_node)::in,
	list(pol_node)::out,
	int::in,
	int::out
) is det.
process_cr_edge_pair(CR, {FromSet, ToSet}, !Edges, !Nodes, !NodeCount) :-
	!:Edges = [Edge | !.Edges],
	Edge = pol_edge(revoke(CR), From, To),
	FromExists = (pred(F::in) is semidet :-
		F = pol_node(_, FromSet)
	),
	(
	if list.find_first_match(FromExists, !.Nodes, FromRes) then
		From = FromRes
	else
		FromID = from_int(!.NodeCount + 1),
		From = pol_node(FromID, FromSet),
		!:Nodes = [From | !.Nodes],
		!:NodeCount = !.NodeCount + 1
	),
	ToExists = (pred(T::in) is semidet :-
		T = pol_node(_, ToSet)
	),
	(
	if list.find_first_match(ToExists, !.Nodes, ToRes) then
		To = ToRes
	else
		ToID = from_int(!.NodeCount + 1),
		To = pol_node(ToID, ToSet),
		!:Nodes = [To | !.Nodes],
		!:NodeCount = !.NodeCount + 1
	).

:- pred ca_pc_edge_pair(
	set(set(role_dynamic))::in,
	set(precond_role(role_dynamic))::in,
	role_dynamic::in,
	{set(role_dynamic), set(role_dynamic)}::out
) is nondet.
ca_pc_edge_pair(NodesPool, Preconds, Target, {FromSet, ToSet}) :-
	ca_edge_pair(NodesPool, Target, {FromSet, ToSet}),
	set.filter_map(pred(pos(R)::in, R::out) is semidet, Preconds, Pos),
	set.subset(Pos, FromSet),
	set.filter_map(pred(neg(R)::in, R::out) is semidet, Preconds, Negs),
	utils.disjoint(FromSet, Negs).

:- pred ca_edge_pair(
	set(set(role_dynamic))::in,
	role_dynamic::in,
	{set(role_dynamic), set(role_dynamic)}::out
) is nondet.
ca_edge_pair(NodesPool, Target, {FromSet, ToSet}) :-
	edge_pair(NodesPool, Target, {FromSet, ToSet}).

:- pred cr_edge_pair(
	set(set(role_dynamic))::in,
	role_dynamic::in,
	{set(role_dynamic), set(role_dynamic)}::out
) is nondet.
cr_edge_pair(NodesPool, Target, {FromSet, ToSet}) :-
	edge_pair(NodesPool, Target, {ToSet, FromSet}).

:- pred edge_pair(
	set(set(role_dynamic))::in,
	role_dynamic::in,
	{set(role_dynamic), set(role_dynamic)}::out
) is nondet.
edge_pair(NodesPool, DiffRole, {SmallerSet, LargerSet}) :-
	member(SmallerSet, NodesPool),
	member(LargerSet, NodesPool),
	member(DiffRole, LargerSet),
	SmallerSet = difference(LargerSet, make_singleton_set(DiffRole)).

admins_for_edge(pol_edge(Action, _, _), Policy, Result, !IO) :-
	(
		(
			Action = assign(ca(Admin, _, _))
		;
			Action = revoke(cr(role(Admin), _))
		),
		users_with_role(Admin, Policy, Users, !IO),
		(
			if list.length(Users) > 0 then
				Result = yes(Users)
			else
				Result = no
		)
	;
		Action = revoke(cr(bool(t), _)),
		Result = yes(["[Any User]"])
	;
		Action = revoke(cr(bool(f), _)),
		Result = no
	).

:- func power_set(list(T)::in) = (set(set(T))::out) is det.
power_set(In) = solutions_set(subset(In)).

% TODO: Consider replacing this with set.subset
:- pred subset(list(T)::in, set(T)::out) is multi.
subset(Set, Subset) :- 
	(
		Set = [],
		Subset = set.init
	;
		Set = [_|_],
		Subset = set.init
	;
		Set = [Set_Head|Set_Tail],
		Subset = set.insert(Subset_Tail, Set_Head),
		subset(Set_Tail, Subset_Tail)
	;
		Set = [_|Set_Tail],
		Subset = set.insert(Subset_Tail, Subset_Head),
		list.append(_, [Subset_Head|Anon_Tail], Set_Tail),
		subset(Anon_Tail, Subset_Tail)
	).

