:- module policy.parse.

:- interface.

:- pred parse_rules_string(string::in, list(rule_static)::out) is det.

:- implementation.

:- import_module char.

parse_rules_string(InputString, OutputRules) :-
	Lines = 
		string.words_separator(
			is_newline, 
			InputString
		),
	list.filter_map(
		pred(Line::in, PTerm::out) is semidet :- (
			string.to_char_list(Line, LineChars),
			rule_line(PTerm, LineChars, _)
		), 
		Lines, 
		OutputRules
	).

:- pred is_newline(char::in) is semidet.
is_newline(TestChar) :- TestChar = '\n'.

:- pred rule_line(rule_static::out, list(char)::in, list(char)::out) is semidet.
rule_line(R) -->
	rule_cr(RR) -> [], {R = RR};
	rule_ca(AR) -> [], {R = AR};
	rule_rh(HR) -> [], {R = HR};
	rule_smer(SR) -> [], {R = SR};
	rule_ua(UR) -> [], {R = UR};
	rule_pa(PR), {R = PR}.

:- pred rule_cr(rule_static::out, list(char)::in, list(char)::out) is semidet.
rule_cr(cr(B, N)) --> 
	ws, can_revoke, ws, ['('], ws, get_bool_or_role(B), ws, [','], ws, role_name(N), ws, [')'], ws.

:- pred rule_ca(rule_static::out, list(char)::in, list(char)::out) is semidet.
rule_ca(ca(N1, PB, N2)) -->
	ws, can_assign, ws, ['('], ws, role_name(N1), ws, [','], ws, get_bool_or_preconds(PB), ws, [','], ws, role_name(N2), ws, [')'].

:- pred rule_rh(rule_static::out, list(char)::in, list(char)::out) is semidet.
rule_rh(rh(A, B)) --> 
	ws, rh, ws, ['('], ws, role_name(A), ws, [','], ws, role_name(B), ws, [')'], ws.

:- pred rule_smer(rule_static::out, list(char)::in, list(char)::out) is semidet.
rule_smer(smer(A, B)) -->
	ws, smer, ws, ['('], ws, role_name(A), ws, [','], ws, role_name(B), ws, [')'], ws.

:- pred rule_ua(rule_static::out, list(char)::in, list(char)::out) is semidet.
rule_ua(ua(U, R)) -->
	ws, ua, ws, ['('], ws, user_name(U), ws, [','], ws, role_name(R), ws, [')'], ws.

:- pred rule_pa(rule_static::out, list(char)::in, list(char)::out) is semidet.
rule_pa(pa(R, P, O)) -->
	ws, pa, ws, ['('], ws, role_name(R), ws, [','], ws, permission_name(P), ws, [','], ws, object_name(O), ws, [')'], ws.

can_revoke --> ['c', 'a', 'n', '_', 'r', 'e', 'v', 'o', 'k', 'e'].

can_assign --> ['c', 'a', 'n', '_', 'a', 's', 's', 'i', 'g', 'n'].

rh --> ['R', 'H'].

smer --> ['S', 'M', 'E', 'R'].

ua --> ['U', 'A'].

pa --> ['P', 'A'].

% Whitespace; via http://adventuresinmercury.blogspot.com/2011/10/parsing-expression-grammars-part-2.html
ws --> ([W], {is_whitespace(W)}) -> ws; [].

get_bool_or_role(BoR) --> 
	bool(B) -> {BoR = bool(B)};
	role_name(R), {BoR = role(R)}.

:- pred bool(boolean::out, list(char)::in, list(char)::out) is semidet.
bool(B) --> 
	['t', 'r', 'u', 'e'] -> {B = t};
	['f', 'a', 'l', 's', 'e'], {B = f}.

role_name(R) --> alnum_name(R).
user_name(U) --> alnum_name(U).

:- pred alnum_name(string::out, list(char)::in, list(char)::out) is semidet.
alnum_name(string.from_char_list(R)) -->
	alnum_rec(R).

% Partly via: http://like-a-boss.net/write-yourself-a-scheme/07-dcgs-and-parsing-continued.html
alnum_rec(Chars) -->
	(
		alnum(H) ->
		{Chars = [H | Rest]}, alnum_rec(Rest);
		{Chars = []}
	).

:- pred alnum(char::out, list(char)::in, list(char)::out) is semidet.
alnum(X) --> [X], { char.is_alnum(X) }.

permission_name(P) --> alpha_name(P).
object_name(O) --> alpha_name(O).

:- pred alpha_name(string::out, list(char)::in, list(char)::out) is semidet.
alpha_name(string.from_char_list(R)) -->
	name_rec(R).

name_rec(Chars) -->
	(
		alpha(H) ->
		{Chars = [H | Rest]}, name_rec(Rest);
		{Chars = []}
	).

:- pred alpha(char::out, list(char)::in, list(char)::out) is semidet.
alpha(X) --> [X], {char.is_alpha(X) }.

:- pred get_bool_or_preconds(bool_or_preconds(role_static)::out, list(char)::in, list(char)::out) is semidet.
get_bool_or_preconds(BoP) --> 
	bool(B) -> {BoP = bool(B)};
	preconds_rec(P), {BoP = preconds(P)}.

:- pred *(pred(T, T), T, T).
:- mode *(pred(in, out) is semidet, in, out) is det.
*(P) --> (P, *(P)) -> []; [].

% This and next predicate also partly via link above
:- pred preconds_rec(set(precond_role(role_static))::out, list(char)::in, list(char)::out) is semidet.
preconds_rec(PL) -->
	(
		precond(P), req_space, preconds_rec(PR) -> {PL = set.insert(PR, P)};
		% TODO: Fix bug where space between final precond and 
		% upcoming comma results in an erroneous item with 
		% value: `pos("")`
		precond(P) -> {PL = make_singleton_set(P)};
		{set.init(PL)}
	).

req_space -->
	(
		[' '] -> ws;
		['\t'], ws
	).

:- pred precond(precond_role(role_static)::out, list(char)::in, list(char)::out) is semidet.
precond(P) -->
	['-'], role_name(R) -> {P = neg(R)};
	role_name(R), {P = pos(R)}.