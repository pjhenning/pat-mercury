:- module policy.conflicts.

:- interface.

:- pred no_smer_conflicts(
	list(smer)::in,
	list(rh)::in
) is semidet.

:- pred no_smer_conflicts_flat(list(smer)::in) is semidet.


:- implementation.

no_smer_conflicts(SMERs, Hierarchy) :-
	all [R1, R2] not (
		member(smer(R1, R2), SMERs),
		senior_in_common(R1, R2, Hierarchy)
	).

no_smer_conflicts_flat(SMERs) :-
	all [R] not member(smer(R, R), SMERs).
