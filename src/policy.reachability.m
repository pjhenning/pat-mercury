:- module policy.reachability.

:- interface.

:- pred reachable(
	goal::in,
	list(transition)::in,
	policy_dynamic::in,
	maybe(list({list(transition), list(list(list(policy_user)))}))::out,
	io::di,
	io::uo
) is det.

:- implementation.

reachable(Goal, Transitions, Policy, Conclusion, !IO) :-
	write_doc(format(Transitions), !IO),
	write_string("\n", !IO),
	filter_map_uas_set(
		pred(ua(U, R)::in, R::out) is semidet :- U = Goal ^ target,
		Policy,
		StartRoles,
		!IO
	),
	write_string("Checking reachability for the following start roles: ", !IO),
	write_doc(format(StartRoles), !IO),
	write_string("\n", !IO),
	solutions(path(StartRoles, Goal ^ g_roles, Transitions), Paths),
	write_string("Resulting paths: ", !IO),
	write_doc(format(Paths), !IO),
	write_string("\n", !IO),
	(
		if list.length(Paths) > 0 then
			list.foldl2(
				pred(
					Path::in,
					!.ExecPaths::in,
					!:ExecPaths::out,
					!.IO2::di,
					!:IO2::uo
				) is det :-
				(
					list.map_foldl(
						pred(Transition::in, ExecResult::out, !.IO3::di, !:IO3::uo) is det :-
							executable(Transition, Policy, ExecResult, !IO3),
							/* ExecResult is a (maybe) list of lists, for example:
								[ListA, ListB, ...] where ListA corresponds to some EdgeA, and
								holds a list of users that can execute that Edge
							*/
						Path,
						/* ExecResults is a list of lists of lists: [J, K, ...] where
							J corresponds to a transition
						*/
						ExecResults,
						!IO2
					),
					PathExecutable = list.all_false(pred(no::in) is semidet, ExecResults),
					(
						if PathExecutable then
							list.filter_map(
								pred(yes(List)::in, List::out) is semidet,
								ExecResults,
								ExecUsersListLists
							),
							!:ExecPaths = [{Path, ExecUsersListLists} | !.ExecPaths]
						else true
					)
				),
				Paths,
				[],
				ExecutablePaths,
				!IO
			),
			(
				if list.length(ExecutablePaths) > 0 then
					Conclusion = yes(ExecutablePaths)
				else
					Conclusion = no
			)
		else
			Conclusion = no
	).

:- pred path(
	set(role_dynamic)::in,
	set(role_dynamic)::in,
	list(transition)::in,
	list(transition)::out
) is nondet.

path(FromRoles, EndRoles, Transitions, Path) :-
	(
		Path = [CurrentTransition],
		member(CurrentTransition, Transitions),
		CurrentTransition ^ t_from ^ n_roles = FromRoles,
		subset(EndRoles, CurrentTransition ^ t_to ^ n_roles)
	;
		Path = [CurrentTransition | Rest],
		member(CurrentTransition, Transitions),
		CurrentTransition ^ t_from ^ n_roles = FromRoles,
		CurrentTransition ^ t_to ^ n_roles = FromNext,
		path(FromNext, EndRoles, Transitions, Rest)
	).

:- pred executable(
	transition::in,
	policy_dynamic::in,
	maybe(list(list(policy_user)))::out,
	io::di,
	io::uo
) is det.
executable(Transition, Policy, Result, !IO) :-
	Transition = transition(_, _, Edges),
	list.map_foldl(
		pred(Edge::in, AdminsResult::out, !.IO2::di, !:IO2::uo) is det :-
			admins_for_edge(Edge, Policy, AdminsResult, !IO2),
		Edges,
		AdminsResults,
		!IO
	),
	Success = list.all_false(
		pred(AdminsResult::in) is semidet :-
			AdminsResult = no,
		AdminsResults
	),
	(
		if Success then
			list.filter_map(
				pred(yes(List)::in, List::out) is semidet,
					AdminsResults,
				UsersLists
			),
			Result = yes(UsersLists)
		else
			Result = no
	).
