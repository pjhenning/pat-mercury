:- module policy.roles.

:- interface.

:- pred with_role(
	role_static::in,
	role_dynamic::out,
	policy_dynamic::in, 
	policy_dynamic::out,
	io::di,
	io::uo
) is det.

:- pred without_role(
	role_dynamic::in,
	policy_dynamic::in,
	policy_dynamic::out,
	io::di,
	io::uo
) is det.

:- pred without_roles(
	set(role_dynamic)::in,
	policy_dynamic::in,
	policy_dynamic::out,
	io::di,
	io::uo
) is det.

:- pred with_pcrole(
	precond_role(role_static)::in,
	set(precond_role(role_dynamic))::in,
	set(precond_role(role_dynamic))::out,
	policy_dynamic::in,
	policy_dynamic::out,
	io::di,
	io::uo
) is det.

:- pred preconds_dyn_to_static(
	set(precond_role(role_dynamic))::in,
	set(precond_role(role_static))::out
) is det.

:- func get_occurrence_rule(role_occurrence::in, io::di, io::uo) =
	(rule_dynamic::out) is det.

:- pred with_occurrence(
	role_dynamic::in,
	role_occurrence::in,
	io::di,
	io::uo
) is det.

:- pred without_occurrence(
	role_dynamic::in,
	role_occurrence::in,
	io::di,
	io::uo
) is det.

:- pred without_precond_occurrences(
	set(precond_role(role_dynamic))::in,
	ca_ref::in,
	io::di,
	io::uo
) is det.

:- pred filter_map_ca_occurrences(
	pred(role_occurrence, role_dynamic, ca, T)::in(pred(in, in, in, out) is semidet),
	list(role_dynamic)::in,
	list(T)::out,
	io::di,
	io::uo
) is det.

:- pred filter_roles_by_occurrences(
	pred(list(role_occurrence))::in(pred(in) is semidet),
	list(role_dynamic)::in,
	set(role_dynamic)::out,
	io::di,
	io::uo
) is det.

:- pred filter_roles_by_occurrences(
	pred(list(role_occurrence))::in(pred(in) is semidet),
	list(role_dynamic)::in,
	set(role_dynamic)::out,
	set(role_dynamic)::out,
	io::di,
	io::uo
) is det.

:- pred filter_roles_by_occurrences_rules(
	pred(list({role_occurrence, rule_dynamic}))::in(pred(in) is semidet),
	list(role_dynamic)::in,
	set(role_dynamic)::out,
	io::di,
	io::uo
) is det.

:- pred users_with_role(
	role_dynamic::in,
	policy_dynamic::in,
	list(policy_user)::out,
	io::di,
	io::uo
) is det.

:- implementation.

with_role(RoleName, Role, !Policy, !IO) :-
	list.filter(
		pred(P_Role::in) is semidet :- P_Role = role_dynamic(RoleName, _),
		!.Policy ^ roles,
		Matches
	),
	(
		if Matches = [RoleMatch | _] then
			Role = RoleMatch
		else
			new_mutvar([], Occurrences, !IO),
			Role = role_dynamic(RoleName, Occurrences),
			!Policy ^ roles := [Role | !.Policy ^ roles]
	).

without_role(Role, !Policy, !IO) :-
	list.delete_all(!.Policy ^ roles, Role, RolesNext),
	!Policy ^ roles := RolesNext,
	without_cas_matching(
		pred(CA::in) is semidet :-
		(
			CA = ca(Role, _, _)
		;
			CA = ca(_, _, Role)
		),
		!Policy,
		!IO
	),
	without_crs_matching(
		pred(CR::in) is semidet :-
		(
			CR = cr(role(Role), _)
		;
			CR = cr(_, Role)
		),
		!Policy,
		!IO
	).
	% remove all rh rules with Role?
	% remove all smer rules with Role?
	% remove all ua rules with Role?
	% remove all pa rules with Role?

without_roles(Roles, !Policy, !IO) :-
	set.foldl2(without_role, Roles, !Policy, !IO).

with_pcrole(PCR, !Preconds, !Policy, !IO) :- 
	(
		PCR = pos(RoleName),
		with_role(RoleName, RoleDyn, !Policy, !IO),
		Precond = pos(RoleDyn),
		set.insert(Precond, !Preconds)
	;
		PCR = neg(RoleName),
		with_role(RoleName, RoleDyn, !Policy, !IO),
		Precond = neg(RoleDyn),
		set.insert(Precond, !Preconds)
	).

preconds_dyn_to_static(Preconds, PrecondNames) :-
	set.map(
		pred(Precond::in, Name::out) is det :-
		(
			Precond = pos(P),
			Name = pos(P ^ name)
		;
			Precond = neg(P),
			Name = neg(P ^ name)
		),
		Preconds,
		PrecondNames
	).

get_occurrence_rule(Occurrence, !IO) = Rule :-
	(
		(
			Occurrence = ca_admin(CA_Ref)
		;
			Occurrence = pos_precond(CA_Ref)
		;
			Occurrence = neg_precond(CA_Ref)
		;
			Occurrence = ca_target(CA_Ref)
		),
		Rule = get_rule_ca(CA_Ref, !.IO, !:IO)
	;
		(
			Occurrence = cr_admin(CR_Ref)
		;
			Occurrence = cr_target(CR_Ref)
		),
		Rule = get_rule_cr(CR_Ref, !.IO, !:IO)
	;
		(
			Occurrence = rh_senior(RH_Ref)
		;
			Occurrence = rh_junior(RH_Ref)
		),
		Rule = get_rule_rh(RH_Ref, !.IO, !:IO)
	).

with_occurrence(Role, Occurrence, !IO) :-
	get_mutvar(Role ^ occurrences, Occurrences, !IO),
	set_mutvar(Role ^ occurrences, [Occurrence | Occurrences], !IO).

without_occurrence(Role, Occurrence, !IO) :-
	role_dynamic(_, OccurrencesRef) = Role,
	get_mutvar(OccurrencesRef, OccurrencesPrev, !IO),
	list.foldl2(
		pred(O::in, !.Rem::in, !:Rem::out, !.IO2::di, !:IO2::uo) is det :-
		(
			occurrence_static(O, A, !IO2),
			occurrence_static(Occurrence, B, !IO2),
			(
				if A \= B then
					!:Rem = [O | !.Rem]
				else true
			)
		),
		OccurrencesPrev,
		[],
		OccurrencesNext,
		!IO
	),
	set_mutvar(OccurrencesRef, OccurrencesNext, !IO).

:- pred occurrence_static(role_occurrence::in, role_occurrence_static::out, io::di, io::uo) is det.
occurrence_static(O, S, !IO) :-
	(
		O = ca_admin(CA_Ref),
		CA = get_ca(CA_Ref, !.IO, !:IO),
		S = ca_admin(CA)
	;
		O = pos_precond(CA_Ref),
		CA = get_ca(CA_Ref, !.IO, !:IO),
		S = pos_precond(CA)
	;
		O = neg_precond(CA_Ref),
		CA = get_ca(CA_Ref, !.IO, !:IO),
		S = neg_precond(CA)
	;
		O = ca_target(CA_Ref),
		CA = get_ca(CA_Ref, !.IO, !:IO),
		S = ca_target(CA)
	;
		O = cr_admin(CR_Ref),
		CR = get_cr(CR_Ref, !.IO, !:IO),
		S = cr_admin(CR)
	;
		O = cr_target(CR_Ref),
		CR = get_cr(CR_Ref, !.IO, !:IO),
		S = cr_target(CR)
	;
		O = rh_senior(RH_Ref),
		RH = get_rh(RH_Ref, !.IO, !:IO),
		S = rh_senior(RH)
	;
		O = rh_junior(RH_Ref),
		RH = get_rh(RH_Ref, !.IO, !:IO),
		S = rh_junior(RH)
	).

without_precond_occurrences(Preconds, CA_Ref, !IO) :-
	set.foldl(
		pred(Precond::in, !.IO2::di, !:IO2::uo) is det :-
		(
			pos(Role) = Precond,
			without_occurrence(Role, pos_precond(CA_Ref), !IO2)
		;
			neg(Role) = Precond,
			without_occurrence(Role, neg_precond(CA_Ref), !IO2)
		),
		Preconds,
		!IO
	).

filter_map_ca_occurrences(Test, Roles, OutValues, !IO) :-
	list.foldl2(
		pred(Role::in, !.OutVals::in, !:OutVals::out, !.IO2::di, !:IO2::uo) is det :-
		(
			get_mutvar(Role ^ occurrences, Occurrences, !IO2),
			list.foldl2(
				pred(Occurrence::in, !.OV::in, !:OV::out, !.IO3::di, !:IO3::uo) is det :-
				(
					(
						(
							Occurrence = ca_admin(CA_Ref)
						;
							Occurrence = pos_precond(CA_Ref)
						;
							Occurrence = neg_precond(CA_Ref)
						;
							Occurrence = ca_target(CA_Ref)
						),
						try_match_ca(
							Test,
							Occurrence,
							Role,
							get_ca(CA_Ref, !.IO3, !:IO3),
							!OV
						)
					;
						(
							Occurrence = cr_admin(_)
						;
							Occurrence = cr_target(_)
						),
						!:OV = !.OV
					;
						(
							Occurrence = rh_senior(_)
						;
							Occurrence = rh_junior(_)
						),
						!:OV = !.OV
					)
				),
				Occurrences,
				!OutVals,
				!IO2
			)
		),
		Roles,
		[],
		OutValues,
		!IO
	).

filter_roles_by_occurrences(Test, Roles, Filtered, !IO) :-
	list.foldl2(
		pred(Role::in, !.Filt::in, !:Filt::out, !.IO2::di, !:IO2::uo) is det :-
		(
			get_mutvar(Role ^ occurrences, Occurrences, !IO2),
			(
				if Test(Occurrences) then
					set.insert(Role, !Filt)
				else
					!:Filt = !.Filt
			)
		),
		Roles,
		set.init,
		Filtered,
		!IO
	).

filter_roles_by_occurrences(Test, Roles, YesRoles, NoRoles, !IO) :-
	list.foldl3(
		pred(
			Role::in,
			!.Yes::in,
			!:Yes::out,
			!.No::in,
			!:No::out,
			!.IO2::di,
			!:IO2::uo
		) is det :-
		(
			get_mutvar(Role ^ occurrences, Occurrences, !IO2),
			(
				if Test(Occurrences) then
					set.insert(Role, !Yes)
				else
					set.insert(Role, !No)
			)
		),
		Roles,
		set.init,
		YesRoles,
		set.init,
		NoRoles,
		!IO
	).

filter_roles_by_occurrences_rules(Test, Roles, Filtered, !IO) :-
	list.foldl2(
		pred(Role::in, !.Filt::in, !:Filt::out, !.IO2::di, !:IO2::uo) is det :-
		(
			get_mutvar(Role ^ occurrences, Occurrences, !IO2),
			list.map_foldl(
				pred(Occurrence::in, {Occurrence, Rule}::out, I::di, O::uo) is det :-
					Rule = get_occurrence_rule(Occurrence, I, O),
				Occurrences,
				OccurrenceTuples,
				!IO2
			),
			(
				if Test(OccurrenceTuples) then
					set.insert(Role, !Filt)
				else
					!:Filt = !.Filt
			)
		),
		Roles,
		set.init,
		Filtered,
		!IO
	).

users_with_role(Role, Policy, Users, !IO) :-
	filter_map_uas_set(
		pred(UA::in, User::out) is semidet :-
			UA = ua(User, Role),
		Policy,
		UsersSet,
		!IO
	),
	Users = set.to_sorted_list(UsersSet).

