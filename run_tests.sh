#!/usr/bin/env bash

# ./clean_all.sh
cd src && mmc --infer-all --make libpolicy
cd .. && mmc --infer-all --search-lib-files-dir ./src --init-file ./src/policy.init --link-object ./src/libpolicy.a --make pat_tests
./pat_tests
